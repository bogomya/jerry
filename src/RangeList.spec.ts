import { RangeList, Segment } from "./RangeList";

describe("RangeList", () => {
  describe("Adding new segments", () => {
    it("should store a new segment", () => {
      const segment: Segment = [1, 5];
      const list = new RangeList().add(segment);
      expect(list.entries()).toEqual([segment]);
    });

    it("should store non-intersecting segments separately", () => {
      const segment1: Segment = [10, 20];
      const segment2: Segment = [1, 5];
      const list = new RangeList().add(segment1).add(segment2);
      expect(list.entries()).toHaveLength(2);
    });

    it("should ignore zero-length segments", () => {
      const segment1: Segment = [10, 20];
      const emptySegment: Segment = [20, 20];
      const list = new RangeList()
        .add(segment1)
        .add(emptySegment)
      ;
      expect(list.entries()).toHaveLength(1);
    });

    it("should extend right bound of a stored segment", () => {
      const segment1: Segment  = [1, 5];
      const segment2: Segment = [10, 20];
      const extendingSegment: Segment = [20, 21];
      const list = new RangeList()
        .add(segment1)
        .add(segment2)
        .add(extendingSegment)
      ;
      expect(list.entries()).toEqual([segment1, [10, 21]]);
    });

    it("should extend left bound of a stored segment", () => {
      const segment1: Segment  = [1, 5];
      const segment2: Segment = [10, 21];
      const extendingSegment: Segment = [8, 12];
      const list = new RangeList()
        .add(segment1)
        .add(segment2)
        .add(extendingSegment)
      ;
      expect(list.entries()).toEqual([segment1, [8, 21]]);
    });

    it("should skip segments, if they are already covered in a range", () => {
      const segment1: Segment  = [1, 5];
      const segment2: Segment = [10, 21];
      const alreadyCoveredSegment: Segment = [2, 4];
      const list = new RangeList()
        .add(segment1)
        .add(segment2)
        .add(alreadyCoveredSegment)
      ;
      expect(list.entries()).toEqual([segment1, [10, 21]]);
    });

    describe("Removing segments", () => {
      it("should ignore zero-length segments", () => {
        const segment1: Segment = [1, 8];
        const segment2: Segment = [10, 21];
        const emptySegment: Segment = [10, 10];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
          .remove(emptySegment)
        ;
        expect(list.entries()).toEqual([segment1, segment2]);
      });

      it("Should remove existed segments", () => {
        const segment1: Segment  = [1, 8];
        const segment2: Segment  = [10, 21];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
          .remove(segment1)
        ;
        expect(list.entries()).toEqual([segment2]);
      });

      it("should cut left bound of a stored segment", () => {
        const segment1: Segment  = [1, 8];
        const segment2: Segment  = [10, 21];
        const deletedSegment: Segment = [10, 11];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
          .remove(deletedSegment)
        ;
        expect(list.entries()).toEqual([segment1, [11, 21]]);
      });

      it("should cut right bound of a stored segment", () => {
        const segment1: Segment  = [1, 8];
        const segment2: Segment  = [10, 21];
        const deletedSegment: Segment = [17, 23];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
          .remove(deletedSegment)
        ;
        expect(list.entries()).toEqual([segment1, [10, 17]]);
      });

      it("should split stored segment, if it include a deleted segment", () => {
        const segment1: Segment  = [1, 8];
        const segment2: Segment  = [11, 21];
        const deletedSegment: Segment = [15, 17];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
          .remove(deletedSegment)
        ;
        expect(list.entries()).toEqual([segment1, [11, 15], [17, 21]]);
      });

      it("should remove stored segment, if it's included in a deleted segment", () => {
        const segment1: Segment  = [1, 8];
        const segment2: Segment  = [11, 15];
        const segment3: Segment = [17, 21];
        const deletedSegment: Segment = [3, 19];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
          .add(segment3)
          .remove(deletedSegment)
        ;
        expect(list.entries()).toEqual([[1, 3], [19, 21]]);
      });
    });

    describe("Printing segments", () => {
      it("should print stored segments", () => {
        const segment1: Segment  = [1, 8];
        const segment2: Segment  = [11, 21];
        const list = new RangeList()
          .add(segment1)
          .add(segment2)
        ;
        expect(list.print()).toEqual("[1, 8) [11, 21)");
      });

      it("should return empty string, if there are no stored segments", () => {
        const list = new RangeList();
        expect(list.print()).toEqual("");
      });
    });
  });
});
