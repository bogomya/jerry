import { Range } from "./Range";

describe("Range", () => {
  describe("Creating new ranges", () => {
    it("should create a new segment", () => {
      const from = 5;
      const to = 10;
      const range = new Range(from, to);
      expect(range.from).toEqual(from);
      expect(range.to).toEqual(to);
    });

    it("should normalize boundaries", () => {
      const from = 10;
      const to = 5;
      const range = new Range(from, to);
      expect(range.from).toEqual(to);
      expect(range.to).toEqual(from);
    });

    it("should return range length", () => {
      expect(new Range(2, 10).length).toEqual(8);
      expect(new Range(10, 2).length).toEqual(8);
      expect(new Range(10, 10).length).toEqual(0);
    });

    describe("Working with coordinates", () => {
      it("should check if range contain a point", () => {
        expect(new Range(2, 10).contain(2)).toBeTruthy();
        expect(new Range(2, 10).contain(5)).toBeTruthy();
        expect(new Range(2, 10).contain(12)).toBeFalsy();
      });

      it("should check if a range is before another range", () => {
        expect(new Range(5, 10).isBefore(new Range(15, 20))).toBeTruthy();
        expect(new Range(5, 10).isBefore(new Range(1, 3))).toBeFalsy();
        expect(new Range(5, 10).isBefore(new Range(7, 12))).toBeFalsy();
        expect(new Range(5, 10).isBefore(new Range(1, 7))).toBeFalsy();
        expect(new Range(5, 10).isBefore(new Range(1, 15))).toBeFalsy();
      });

      it("should check if a range is after another range", () => {
        expect(new Range(15, 20).isAfter(new Range(5, 10))).toBeTruthy();
        expect(new Range(15, 20).isAfter(new Range(25, 40))).toBeFalsy();
        expect(new Range(15, 20).isAfter(new Range(17, 25))).toBeFalsy();
        expect(new Range(15, 20).isAfter(new Range(10, 17))).toBeFalsy();
        expect(new Range(15, 20).isAfter(new Range(10, 35))).toBeFalsy();
      });

      it("should check if a range include another range", () => {
        expect(new Range(10, 20).include(new Range(12, 15))).toBeTruthy();
        expect(new Range(10, 20).include(new Range(10, 20))).toBeTruthy();
        expect(new Range(10, 20).include(new Range(5, 15))).toBeFalsy();
        expect(new Range(10, 20).include(new Range(15, 25))).toBeFalsy();
        expect(new Range(10, 20).include(new Range(5, 30))).toBeFalsy();
      });
    });
  });
});
