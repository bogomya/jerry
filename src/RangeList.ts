import { Point, Range } from "./Range";

export type Segment = [Point, Point];

export class RangeList {
  private ranges: Range[] = [];

  public add(segment: Segment): RangeList {
    let range = new Range(...segment);
    if (!range.length) {
      return this;
    }

    const ranges = [];
    for (let i = 0; i < this.ranges.length; ++i) {
      const item = this.ranges[i];

      if (range.isBefore(item)) {
        ranges.push(range, ...this.ranges.slice(i));
        this.ranges = ranges;
        return this;
      }

      if (range.isAfter(item)) {
        ranges.push(item);
      }

      if (item.include(range)) {
        return this;
      }

      if (item.contain(range.from)) {
        range = new Range(item.from, range.to);
      }

      if (item.contain(range.to)) {
        range = new Range(range.from, item.to);
      }
    }

    ranges.push(range);
    this.ranges = ranges;
    return this;
  }

  public remove(segment: Segment): RangeList {
    const range = new Range(...segment);
    if (!range.length) {
      return this;
    }

    const ranges = [];
    for (let i = 0; i < this.ranges.length; ++i) {
      const item = this.ranges[i];
      if (range.isBefore(item)) {
        ranges.push(...this.ranges.slice(i));
        break;
      }

      if (range.isAfter(item)) {
        ranges.push(item);
      }

      if (range.include(item)) {
        continue;
      }

      if (item.contain(range.from)) {
        const section = new Range(item.from, range.from);
        if (section.length) {
          ranges.push(section);
        }
      }

      if (item.contain(range.to)) {
        const section = new Range(range.to, item.to);
        if (section.length) {
          ranges.push(section);
        }
      }
    }

    this.ranges = ranges;
    return this;
  }

  public entries(): Segment[] {
    return this.ranges.map((range) => [range.from, range.to]);
  }

  public print(): string {
    return this.ranges.join(" ");
  }
}
