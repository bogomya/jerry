export type Point = number;

export class Range {
  public from: Point;
  public to: Point;

  public constructor(from: Point, to: Point) {
    this.from = Math.min(from, to);
    this.to = Math.max(from, to);
  }

  get length() {
    return Math.abs(this.from - this.to);
  }

  public contain(point: Point) {
    return point >= this.from && point <= this.to;
  }

  public isBefore(range: Range) {
    return this.to < range.from;
  }

  public isAfter(range: Range) {
    return this.from > range.to;
  }

  public include(range: Range) {
    return this.contain(range.from) && this.contain(range.to);
  }

  public toString() {
    return `[${this.from}, ${this.to})`;
  }
}
