## Jerry.AI

Test assignment for Jerry.AI

### Requirements

You need to have installed:

* NodeJS 8+

### Stack
* TypeScript, TSLint
* Jest

##### Features:
O(n) implementation for RangeList add/remove methods 

Unit tests with Jest

### Setup
Run ```npm install``` and then ```npm run test``` or ```npm run coverage```

##### Enjoy 🚀
